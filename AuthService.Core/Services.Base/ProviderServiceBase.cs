using AuthService.DataAccess.Providers;
using AuthService.IdpServices;
using AuthService.IdpServices.IdpServices.Base;
using Common.Utility.CommonSettings;
using Common.Utility.Exceptions.HttpGeneric;
using Microsoft.Extensions.Options;

namespace AuthService.Core.Services.Base
{
    public abstract class ProviderServiceBase : ServiceBase
    {   
        
        // Private access
        protected IdpSettings _idpSettings { get; set; }
        private string _server_url { get; set;}

        /// Abstract constructor ***     
        public ProviderServiceBase
        (
            IOptions<AppSettings> settings
        )
        : base(settings)
        {
            this.SetIdpServices();
        }

        /// <summary>
        /// Private settings check and load.
        /// <summary>
        private void SetIdpServices()
        {   
            if(string.IsNullOrEmpty(base._settings.Security.ServerUrl)) throw new ConflictException("Settings missing server url!");
            if(string.IsNullOrEmpty(base._settings.Security.RealmName)) throw new ConflictException("Settings missing realm!");
            if(string.IsNullOrEmpty(base._settings.Security.ClientId)) throw new ConflictException("Settings missing client!");
            if(string.IsNullOrEmpty(base._settings.Security.ClientAdmin)) throw new ConflictException("Settings missing admin client!");
            if(string.IsNullOrEmpty(base._settings.Security.ClientAdminSecret)) throw new ConflictException("Settings missing admin secret!");

            IdpSettings idpSettings = new IdpSettings
            (
                base._settings.Security.ServerUrl,
                base._settings.Security.RealmName,
                base._settings.Security.ClientId,
                base._settings.Security.ClientAdmin,
                base._settings.Security.ClientAdminSecret
            );
            /// Save settings
            this._idpSettings = idpSettings;
        }

    }
}