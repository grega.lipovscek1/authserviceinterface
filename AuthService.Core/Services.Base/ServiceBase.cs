using Common.Utility.CommonSettings;
using Microsoft.Extensions.Options;


namespace AuthService.Core.Services.Base
{   
    /// <summary>
    /// Service base abstract class model!
    /// <summary>
    public abstract class ServiceBase
    {   
        /// Access protected
        protected readonly AppSettings _settings;
        
        /// Constructor ***
        /// Setting construction
        public ServiceBase(IOptions<AppSettings> settings)
        {
             _settings = settings.Value;
        }
    }

}