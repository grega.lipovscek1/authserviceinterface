using AuthService.IdpServices.Contracts;

namespace AuthService.Core.Login.Services.Register.Services
{
    public interface IRegisterService
    {   

        /// <summary>
        /// Register user in identity provider.
        /// <summary>
        public Task<IdentityProviderUserProfileResponse> UserRegisterService(string email, string password, string? userName);

        /// <summary>
        /// Resend user verifiy email by email.
        /// <summary>
        public Task<bool> ResendUserVerifyEmail(string email);
        
    }
}