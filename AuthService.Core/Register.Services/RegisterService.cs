using AuthService.Core.Models;
using AuthService.Core.Services.Base;
using AuthService.DataAccess.Entities;
using AuthService.DataAccess.Providers;
using AuthService.IdpServices;
using AuthService.IdpServices.Contracts;
using AuthService.IdpServices.Contracts.Models;
using Common.Utility.CommonSettings;
using Common.Utility.Exceptions.HttpGeneric;
using Microsoft.Extensions.Options;

namespace AuthService.Core.Login.Services.Register.Services
{   
    internal class CoreRSRsClass : IClassSett 
    {
        public const string CLASSCODE = "AS-Core-R.S-ReSe";
        public static string ToCode(string functionCode)
        {
            return (string) CoreRSRsClass.CLASSCODE + " [ " + functionCode + " ]";
        }
    }
    public class RegisterService : ProviderServiceBase, IRegisterService
    {   
        /// Protected access   
        protected IdpAdminProviderServices _idpAdminProviderServices { get; set; }
        protected AccountEntityProvider _accountEntityProvider { get; set; }

        /// Constructor ***
        public RegisterService
        (
            IOptions<AppSettings> settings,
            IdpAdminProviderServices idpAdminProviderServices,
            AccountEntityProvider accountEntityProvider
        )
        : base(settings)
        {   
            this._idpAdminProviderServices = idpAdminProviderServices;
            this._accountEntityProvider = accountEntityProvider;
        }
        /// Access Public

        public async Task<IdentityProviderUserProfileResponse> UserRegisterService(string email, string password, string? userName)
        {   
            this._idpAdminProviderServices.Configure(base._idpSettings);


            AccountEntity account = await this._accountEntityProvider.GetAllUserDataByEmail(email);

            /// Check if user exists in idp provider
            if(await this._idpAdminProviderServices.FetchUserDataByEmail(email) != null)  throw new ConflictException("User already exists"); 

            // Check if user exists in local database
            if(await this._accountEntityProvider.GetAllUserDataByEmail(email) != null) throw new ConflictException("User already exists in local databse"); 

            UserRegistrationModel userRegistrationModel = new UserRegistrationModel()
            {
                UserName = userName,
                Email = email,
                Password = password
            };
            IdentityProviderUserProfileResponse createdUserProfile = await this.UserRegisterServiceRequest(userRegistrationModel);

            AccountEntity accountEntity = new AccountEntity(){
                UserId = createdUserProfile.Id,
                UserName = createdUserProfile.Email,
                Email = createdUserProfile.Email,
                FirstName = createdUserProfile.FirstName,
                LastName = createdUserProfile.LastName
            };
            
            /// Save account to local database
            await this._accountEntityProvider.SaveAsync(accountEntity);
            
            /// Send request to idp form email vefification.
            await this._idpAdminProviderServices.SendUserVerifyEmailRequest(createdUserProfile.Id);

            UserProfileModel userProfile = new UserProfileModel(createdUserProfile);

            return createdUserProfile;         
        }

        public async Task<bool> ResendUserVerifyEmail(string email)
        {   
            this._idpAdminProviderServices.Configure(base._idpSettings);

            IdentityProviderUserProfileResponse userProfile = await this._idpAdminProviderServices.FetchUserDataByEmail(email);
            if(await this._idpAdminProviderServices.SendUserVerifyEmailRequest(userProfile.Id) == true)
            {
                return true;
            }
            
            else throw new InternalServerException(CoreRSRsClass.ToCode("E.20021"));

        } 
        
        
        /// Access Private
        private async Task<IdentityProviderUserProfileResponse> UserRegisterServiceRequest(UserRegistrationModel userRegistrationModel)
        {             
            if(await this._idpAdminProviderServices.IdpUserRegisterService(userRegistrationModel) == true)
            {   
                this._idpAdminProviderServices.Configure(base._idpSettings);

                IdentityProviderUserProfileResponse userProfile = await this._idpAdminProviderServices.FetchUserDataByEmail(userRegistrationModel.Email);
                
                return userProfile;
            }
            else throw new InternalServerException(CoreRSRsClass.ToCode("E.20025/P")); 
        }
        
    /// <class>
    }
}