using AuthService.Core.Services.Base;
using AuthService.DataAccess.Entities;
using AuthService.DataAccess.Providers;
using AuthService.IdpServices;
using AuthService.IdpServices.Contracts;
using AuthService.IdpServices.Contracts.Models;
using Common.Utility.CommonSettings;
using Microsoft.Extensions.Options;

namespace AuthService.Core.Login.Services
{   
    public class LoginService : ProviderServiceBase
    {   
        private readonly AccountEntityProvider _accountEntityProvider;

        private readonly IdpProviderServices _idpProviderServices;


        public LoginService
        (   
            IOptions<AppSettings> settings,
            AccountEntityProvider accountEntityProvider,
            IdpProviderServices idpProviderServices
        )
        : base(settings)
        {
            _accountEntityProvider = accountEntityProvider;

            _idpProviderServices = idpProviderServices;

        }

        public async Task<IdentityProviderCridentialsResponse> UserLoginService(string email, string password)
        {   
            this._idpProviderServices.Configure(base._idpSettings);

            IdentityProviderCridentialsResponse serviceResult = 
                                await this._idpProviderServices.UserLoginIdpProviderService(
                                    new UserLoginModel(email,password,email)
                                );
            
            return serviceResult;
        }

        public async Task<IdentityProviderCridentialsResponse> RefreshTokenService(string refreshToken)
        {   
            this._idpProviderServices.Configure(base._idpSettings);

            IdentityProviderCridentialsResponse serviceResult = 
                                await this._idpProviderServices.RequestNewAccessToken(
                                    new RefreshTokenRequest(refreshToken)
                                );
            
            return serviceResult;
        }

    }
}