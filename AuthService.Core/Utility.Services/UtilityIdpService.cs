using AuthService.Core.Models;
using AuthService.Core.Services.Base;
using AuthService.DataAccess.Providers;
using AuthService.IdpServices;
using AuthService.IdpServices.Contracts;
using Common.Utility.CommonSettings;
using Microsoft.Extensions.Options;

namespace AuthService.Core.Utility.Services
{
    public class UtilityIdpService : ProviderServiceBase
    {   
        protected IdpAdminProviderServices _idpAdminProviderServices { get; set; }
        protected AccountEntityProvider _accountEntityProvider { get; set; }

        /// Constructor ***
        public UtilityIdpService
        (
            IOptions<AppSettings> settings,
            IdpAdminProviderServices idpAdminProviderServices,
            AccountEntityProvider accountEntityProvider
        )
        : base(settings)
        {
            this._idpAdminProviderServices = idpAdminProviderServices;
            this._accountEntityProvider = accountEntityProvider;
        }

        public async Task<string> CheckAdminAccessTokenService()
        {   
            this._idpAdminProviderServices.Configure(base._idpSettings);
            
            return await this._idpAdminProviderServices.CheckAdminTokenIdpServiceProvider();
        }
        public async Task<UserProfileModel> FetchUserProfileByEmail(string email)
        {   
            this._idpAdminProviderServices.Configure(base._idpSettings);
            
            IdentityProviderUserProfileResponse userProfileResponse = await this._idpAdminProviderServices.FetchUserDataByEmail(email);

            UserProfileModel userProfile = new UserProfileModel(userProfileResponse);

            return userProfile;
        }

    /// <class>
    }
}