using AuthService.DataAccess.Entities;

namespace AuthService.Core.User.Services
{
    public interface IUserService
    {   
        /// <summary>
        /// Updates user profile
        /// <summary>
        public Task<bool> UpdateUserProfileService(Guid userId, string firstName, string lastname);

        /// <summary>
        /// Request user profile
        /// <summary>
        public Task<AccountEntity> GetUserDataById(Guid userId);
    }
}