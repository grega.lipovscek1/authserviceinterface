using AuthService.Core.Services.Base;
using AuthService.DataAccess.Entities;
using AuthService.DataAccess.Providers;
using AuthService.IdpServices;
using AuthService.IdpServices.Contracts;
using Common.Utility.CommonSettings;
using Common.Utility.Exceptions.HttpGeneric;
using Microsoft.Extensions.Options;

namespace AuthService.Core.User.Services
{   
    
    public class UserServices : ProviderServiceBase, IUserService
    {   
        private readonly AccountEntityProvider _accountEntityProvider;
        private readonly IdpProviderServices _idpProviderServices;
        private readonly IdpAdminProviderServices _idpAdminProviderServices;
        public UserServices 
        (   
            IOptions<AppSettings> settings,
            AccountEntityProvider accountEntityProvider,
            IdpProviderServices idpProviderServices,
            IdpAdminProviderServices idpAdminProviderServices
        )
        : base(settings)
        {
            this._accountEntityProvider = accountEntityProvider;

            this._idpProviderServices = idpProviderServices;

            this._idpAdminProviderServices = idpAdminProviderServices;

        }
        public async Task<bool> UpdateUserProfileService(Guid userId, string firstName, string lastname)
        {   
            
            ProviderChangeUserProfileRequest providerRequest = new ProviderChangeUserProfileRequest()
            {
                FirstName = firstName,
                LastName = lastname
            };

            this._idpAdminProviderServices.Configure(base._idpSettings);

            await this._idpAdminProviderServices.UpdateUserProfile(userId, providerRequest);

            AccountEntity account = new AccountEntity()
            {
                UserId = new Guid(),
                UserName = "username",
                FirstName = firstName,
                LastName = lastname
            };
            await this._accountEntityProvider.SaveAsync(account);
            return true;
        }

        public async Task<AccountEntity> GetUserDataById(Guid userId)
        {
            AccountEntity result = await this._accountEntityProvider.GetAllUserDataByUserId(userId);

            if(result == null) throw new ConflictException("User with this id do not exists in local database");

            return result;
        }
    }
}