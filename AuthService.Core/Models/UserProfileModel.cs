using AuthService.IdpServices.Contracts;

namespace AuthService.Core.Models
{
    public class UserProfileModel
    {
        public Guid Id { get; set; }
        public long CreatedTimestamp { get; set; }
        public string Username { get; set; }
        public bool Enabled { get; set; }
        public bool Totp { get; set; }
        public bool EmailVerified { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<string> DisableableCredentialTypes { get; set; }
        public List<string> RequiredActions { get; set; }
        public long NotBefore { get; set; }
        public UserProfileAccessModel Access { get; set; }

        public UserProfileModel(IdentityProviderUserProfileResponse response)
        {
            this.Id = response.Id;
            this.CreatedTimestamp = response.CreatedTimestamp;
            this.Enabled = response.Enabled;
            this.Totp = response.Totp;
            this.EmailVerified = response.EmailVerified;
            this.Username = response.Username;
            this.Email = response.Email;
            this.DisableableCredentialTypes = response.DisableableCredentialTypes;
            this.RequiredActions = response.RequiredActions;
            this.NotBefore = response.NotBefore;
            this.Access = new UserProfileAccessModel()
            {
                ManageGroupMembership = response.Access.ManageGroupMembership,
                View = response.Access.View,
                MapRoles = response.Access.MapRoles,
                Impersonate = response.Access.Impersonate,
                Manage = response.Access.Manage,
            };
        }
    }

    public record UserProfileAccessModel
    {
        public bool ManageGroupMembership { get; set; }
        public bool View { get; set; }
        public bool MapRoles { get; set; }
        public bool Impersonate { get; set; }
        public bool Manage { get; set; }
    }
}