using System.Net;
using System.Text.Json;

namespace Common.Utility.CommonResponses
{
    public class ErrorDetails
    {
        private string _message { get; set; }

        public ErrorDetails(string mesage)
        {
            this._message = mesage;
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
