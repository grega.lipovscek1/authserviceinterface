using System.Net;
using System.Runtime.CompilerServices;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;
using Common.Utility.Exceptions.HttpGeneric;
using Common.Utility.CommonResponses;
using Common.Utility.Exceptions.SystemGeneric;

namespace Common.Utility.Middlewares;

public class ExceptionMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<ExceptionMiddleware> _logger;

    public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
    {
        _logger = logger;
        _next = next;
    }

    /// <summary>
    /// Invoke and intercept http context when exception is thrown.
    /// In rerutes it to exception handler.
    /// <summary>
    public async Task InvokeAsync(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (BadRequestException ex)
        {
            _logger.LogError($"Something went wrong: {ex.Exception}");
            await HandleExceptionAsync<ErrorDetails>(httpContext, ex.Exception, HttpStatusCode.BadRequest, ex.ExceptionDetails);
        }
        catch (InternalServerException ex)
        {
            _logger.LogError($"Something went wrong: {ex.Exception}");
            await HandleExceptionAsync<ErrorDetails>(httpContext, ex.Exception, HttpStatusCode.BadRequest, ex.ExceptionDetails);
        }
        catch (ConflictException ex)
        {
            _logger.LogError($"Something went wrong: {ex.Exception}");
            await HandleExceptionAsync<ErrorDetails>(httpContext, ex.Exception, HttpStatusCode.BadRequest, ex.ExceptionDetails);
        }
        catch (UnauthorizedException ex)
        {
            _logger.LogError($"Something went wrong: {ex.Exception}");
            await HandleExceptionAsync<ErrorDetails>(httpContext, ex.Exception, HttpStatusCode.Unauthorized, ex.ExceptionDetails);
        }
        catch (BadGatewayException ex)
        {
            _logger.LogError($"Something went wrong: {ex.Exception}");
            await HandleExceptionAsync<ErrorDetails>(httpContext, ex.Exception, HttpStatusCode.BadGateway, ex.ExceptionDetails);
        }
        catch (ApiException ex)
        {
            _logger.LogError($"Something went wrong: {ex.Exception}");
            await HandleExceptionAsync<ErrorDetails>(httpContext, ex.Exception, ex.StatusCode, ex.ExceptionDetails);
        }

        /// Catch every exception if it is not handlerd thru exceptions.
        catch (Exception ex)
        {
            _logger.LogError($"Something went wrong: {ex}");
            await HandleExceptionAsync<ErrorDetails>(httpContext, ex, HttpStatusCode.InternalServerError, null);
        }
    }

    /// <summary>
    /// Hadles http context when exception is thrown. It sets cosume response message and, if exception details
    /// exists, returs detail json body.
    /// <summary>
    private async Task HandleExceptionAsync<T>(HttpContext context, Exception exception, HttpStatusCode httpStatusCode, T? exceptionDetails)
    {   
        /// Return response as json format.
        context.Response.ContentType = "application/json";

        /// Set response header status code.
        context.Response.StatusCode = (int) httpStatusCode;

        // Set costume response header message.
        context.Response.Headers.Add("Api-response-message", exception.Message);
        
        /// Set response detail error message if neaded!
        string response = "";

        if(exceptionDetails != null)
        {
            response = exceptionDetails?.ToString();
        }

        /// Form and return response!   
        await context.Response.WriteAsync(response);
    }
}

