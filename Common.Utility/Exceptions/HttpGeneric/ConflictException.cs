using Common.Utility.Exceptions.SystemGeneric;

namespace Common.Utility.Exceptions.HttpGeneric
{
    public class ConflictException : ApiExceptionBase
    {   
        public ConflictException(string message): base(message)
        {
            this.Exception = new Exception(message);
        }
    }
}