using System.Net;
using Common.Utility.Exceptions.SystemGeneric;

namespace Common.Utility.Exceptions.HttpGeneric
{   
    /// <summary>
    /// Return message in header. Contains message and 
    /// gateway responded header code.
    /// <summary>
    public class BadGatewayException : ApiExceptionBase
    {   
        public HttpStatusCode GateWayStatusCode { get; set; }

        public BadGatewayException(string message,  HttpStatusCode gateWayStatusCode)
        : base(message)
        {
            this.Exception = new Exception(message + " GwH.: [ " + gateWayStatusCode + " ]");
        }
    }
    /// <class>
}