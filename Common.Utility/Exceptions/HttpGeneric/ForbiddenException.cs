namespace Common.Utility.Exceptions.HttpGeneric
{
    public class ForbiddenException : Exception
    {   
        public Exception Exception { get; set; }
        public ForbiddenException(string message)
        {
            this.Exception = new Exception(message);
        }
    }
}