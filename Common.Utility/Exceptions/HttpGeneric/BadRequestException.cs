using System.Net;
using Common.Utility.Exceptions.SystemGeneric;

namespace Common.Utility.Exceptions.HttpGeneric
{   
    /// <summary>
    /// Return message in header. Contains message and 
    /// gateway responded header code.
    /// <summary>
    public class BadRequestException : ApiExceptionBase
    {   
        public BadRequestException(string message)
        : base(message)
        {
            this.Exception = new Exception(message);
        }
    }
    /// <class>
}