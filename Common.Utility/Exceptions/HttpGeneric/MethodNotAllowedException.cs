namespace Common.Utility.Exceptions.HttpGeneric
{
    public class MethodNotAllowedException : Exception
    {   
        public Exception Exception { get; set; }
        public MethodNotAllowedException(string message)
        {
            this.Exception = new Exception(message);
        }
    }
}