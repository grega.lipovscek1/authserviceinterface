using System.Net;
using Common.Utility.Exceptions.SystemGeneric;

namespace Common.Utility.Exceptions.HttpGeneric
{   
    /// <summary>
    /// Return message in header. Contains message and 
    /// gateway responded header code.
    /// <summary>
    public class UnauthorizedException : ApiExceptionBase
    {   
        public UnauthorizedException(string message)
        : base(message)
        {
            this.Exception = new Exception(message);
        }
    }
    /// <class>
}