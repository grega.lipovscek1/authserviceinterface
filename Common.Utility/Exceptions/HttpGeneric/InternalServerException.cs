using Common.Utility.Exceptions.SystemGeneric;

namespace Common.Utility.Exceptions.HttpGeneric
{
    public class InternalServerException : ApiExceptionBase
    {   
        public InternalServerException(string message)
        : base(message)
        {
        }
    }
}