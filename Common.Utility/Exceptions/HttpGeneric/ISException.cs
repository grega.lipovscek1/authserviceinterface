using System.Net;

namespace Common.Utility.Exceptions.HttpGeneric
{
    public class ISException : Exception
    {   
        public Exception Exception { get; set; }
        public ISException(string message,  string? code)
        {
            this.Exception = new Exception(message + code);
        }
    }
}