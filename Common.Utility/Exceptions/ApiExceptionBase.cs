using Common.Utility.CommonResponses;

namespace Common.Utility.Exceptions.SystemGeneric
{   
    /// <summary>
    /// Api exception base class. It wraps normal exception
    /// adding exceptionDetails flag, default set as false. 
    /// Default is header api response message. 
    /// Api response heder key : Api-response-message
    /// <summary>
    public abstract class ApiExceptionBase : Exception
    {   
        public Exception Exception { get; set; }

        public ErrorDetails ExceptionDetails { get; set; } = null;

        public ApiExceptionBase(string message)
        {   
            this.Exception = new Exception(message);
        }
    }
    /// <class>
}