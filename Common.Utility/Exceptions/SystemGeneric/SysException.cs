using System.Net;

namespace Common.Utility.Exceptions.SystemGeneric
{
    public class SysException : Exception
    {   
        public Exception Exception { get; set; }
        public SysException(string message, string code)
        {
            this.Exception = new Exception(message + " " + code);
        }
    }
}