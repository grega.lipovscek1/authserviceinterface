using Common.Utility.Exceptions.SystemGeneric.SystemGeneric.Base;

namespace Common.Utility.Exceptions.SystemGeneric
{
    public class DatabaseException
    {   
        public Exception Exception { get; set; }
        public DatabaseException(string code, string message)
        {
            this.Exception = new Exception(message+code);
        }
    }
}