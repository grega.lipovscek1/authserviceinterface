using System.Net;

namespace Common.Utility.Exceptions.SystemGeneric
{
    public class ApiDetailsException<T> : ApiExceptionBase
    {   
        public HttpStatusCode StatusCode { get; set; }

        public T errorDetails { get; set; }

        public ApiDetailsException(string message, HttpStatusCode statusCode, T errorDetails)
        : base(message)
        {   
            this.StatusCode = statusCode;

            this.errorDetails = errorDetails;
        }
    }
}