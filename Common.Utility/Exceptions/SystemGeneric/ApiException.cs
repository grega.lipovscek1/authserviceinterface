using System.Net;

namespace Common.Utility.Exceptions.SystemGeneric
{
    public class ApiException : ApiExceptionBase
    {   
        public HttpStatusCode StatusCode { get; set; }

        public ApiException(string message, HttpStatusCode statusCode)
        : base(message)
        {   
            this.StatusCode = statusCode;
        }
    }
}