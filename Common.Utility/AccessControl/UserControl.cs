using System.IdentityModel.Tokens.Jwt;

namespace Common.Utility.AccessControl
{
    public static class UserControl 
    {   
        public static AccessUserModel GetUserData(string tokenBearer)
        {   
            /// Get jtw parts
            string[] bearer = tokenBearer.Split(" ");
            string token = bearer[1];

            /// Read and decode jwt
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken jsonToken = handler.ReadJwtToken(token);
            
            /// Extract user Id ( sub )
            Guid userId;
            Guid.TryParse(jsonToken.Payload.Sub, out userId);

            /// Extract email
            jsonToken.Payload.TryGetValue("email", out object emailClaim);
            string email = emailClaim.ToString();

            /// Extract username 
            jsonToken.Payload.TryGetValue("preferred_username", out object usernameClaim);
            string username = usernameClaim.ToString();

            /// TODO: EXTRACT ROLES, RESOURCES

            AccessUserModel user = new AccessUserModel()
            {
               UserId = userId,
               Email = email,
               UserName = username
            };

            return user;
        }
    }
}