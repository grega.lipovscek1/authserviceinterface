namespace Common.Utility.AccessControl
{
    public record AccessUserModel 
    {
        public Guid UserId { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

       
    }
}