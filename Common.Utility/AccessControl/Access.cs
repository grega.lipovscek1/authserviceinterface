using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;



namespace Common.Utility.AccessControl
{   
    /// <summary>
    /// Access control modul. Authenticates user jwt token.
    // TODO: make user info call for user validation role.
    /// <summary>

    public static class Access 
    {
        public static void AddAccessControl(this IServiceCollection services, IConfiguration configuration)
        {
            try
            {   
                Console.WriteLine(configuration.ToString());

                /// Check appsetting configuration
                if(configuration.GetChildren().Count() <= 0) throw new ArgumentNullException("Configuration empty!");
            }
            catch(ArgumentNullException)
            {
                System.Console.WriteLine("Configuration section empty!");
                throw new ArgumentNullException("Configuration empty!");
            }

            // Add jwt authentication
            services.AddAuthentication(options => 
            {   
                /// Authentication option: bearer.
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;

                /// Authentication option: bearer.
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(o =>
            {   
                /// Set authoroty ( isshuer )
                o.Authority = $"{configuration["ServerUrl"]}/realms/{configuration["RealmName"]}/";
                o.Audience = configuration["Audience"];
                o.RequireHttpsMetadata = configuration.GetValue<bool>("RequireHttps");
                o.SaveToken = true;

                /// Token validation and span
                o.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };
                o.Events = new JwtBearerEvents()
                {
                    OnTokenValidated = async c =>
                    {

                    }
                    
                };
            }
            );

            
        }

    /// <class>
    }
}