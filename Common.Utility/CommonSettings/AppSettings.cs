

namespace Common.Utility.CommonSettings;

public class AppSettings
{
    public DatabaseSettings? Database { get; set; }

    public class DatabaseSettings
    {
        public string? Host { get; set; }
        public string? Port { get; set; }
        public string? Username { get; set; }
        public string? Password { get; set; }
        public string? Database { get; set; }
    }

    public SecuritySettings? Security { get; set; }
    
    public class SecuritySettings
    {
        public string? ServerUrl { get; set; }
        public string? RealmName { get; set; }
        public string? ClientId { get; set; }
        public string? Audience { get; set; }
        public string? RequireHttps { get; set; }
        public string? ClientAdmin { get; set; }
        public string? ClientAdminSecret { get; set; }

    }
    
}

