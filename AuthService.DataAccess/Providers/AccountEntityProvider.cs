using Microsoft.Extensions;
using Microsoft.EntityFrameworkCore;
using AuthService.DataAccess;
using AuthService.DataAccess.Entities;
using Npgsql;
using Common.Utility.Exceptions.HttpGeneric;
using Common.Utility.Exceptions.SystemGeneric;
using AuthService.DataAccess.Providers.Base;

namespace AuthService.DataAccess.Providers
{
    public class AccountEntityProvider : ProviderBase
    {   

        /// <summary>
        /// AddAndSaveAync
        /// <summary>
        public async Task<bool> SaveAsync(AccountEntity account)
        {          
            try
            {
                _database.Account.Add(account);

                await _database.SaveChangesAsync();

                return true;
            }
            catch (Exception e)
            {
                if (e.InnerException is PostgresException pgException)
                {
                    throw new InternalServerException(pgException.SqlState);
                }
                else throw new InternalServerException("Somenthing else went wrong");
            }
        }

        public async Task<AccountEntity> GetAllUserDataByUserId(Guid userId)
        {
            try
            {
                AccountEntity result = _database.Account.FirstOrDefault(x => x.UserId == userId);

                return result;
            }
            catch (Exception e)
            {
                if (e.InnerException is PostgresException pgException)
                {
                    throw new InternalServerException(pgException.SqlState);
                }
                else throw new InternalServerException("Somenthing else went wrong");
            }
        }

        public async Task<AccountEntity> GetAllUserDataByEmail(string email)
        {
            try
            {
                AccountEntity result = _database.Account.FirstOrDefault(x => x.Email == email);

                return result;
            }
            catch (Exception e)
            {
                if (e.InnerException is PostgresException pgException)
                {
                    throw new InternalServerException(pgException.SqlState);
                }
                else throw new InternalServerException("Somenthing else went wrong");
            }
        }

        
        /// Constructor ***
        public AccountEntityProvider(ApplicationDbContext db)
        :base(db)
        {
        }
        
       
    }
}