namespace AuthService.DataAccess.Providers.Base
{
    public abstract class ProviderBase 
    {
        protected readonly ApplicationDbContext _database;
        public ProviderBase(ApplicationDbContext db)
        {
            _database = db;
        }
    }
}