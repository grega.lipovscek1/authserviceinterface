using System;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AuthService.DataAccess.Entities;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using Common.Utility.CommonSettings;

namespace AuthService.DataAccess
{
    public class ApplicationDbContext : DbContext
    {   
        protected readonly AppSettings _settings;

        public ApplicationDbContext(IOptions<AppSettings> settings)
        {   
            _settings = settings.Value;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_settings.Database == null) throw new Exception("Database settings not found");

            optionsBuilder.UseNpgsql(
                $"Host={_settings.Database.Host!};" +
                $"Database={_settings.Database.Database!};" +
                $"Username={_settings.Database.Username!};" +
                $"Password={_settings.Database.Password!}");

        }

        public DbSet<AccountEntity> Account => Set<AccountEntity>();

    }

    
}