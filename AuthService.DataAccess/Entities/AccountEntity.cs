using System;
using System.ComponentModel.DataAnnotations;

namespace AuthService.DataAccess.Entities
{
    public class AccountEntity : EntityBase
    {   
        [Key]
        public Guid UserId { get; set; }
        public string? UserName { get; set; }

        public string Email { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }
    }
}