using AuthService.IdpServices.Contracts;
using AuthService.IdpServices.Contracts.Models;

namespace AuthService.IdpServices
{
    public interface IIdpAdminProviderServices
    {   
        /// <summay>
        /// Register user in identity provider. Username and password are required.
        /// <summay>
        /// @var UserRegistrationModel
        /// <returs = bool> 
        public Task<bool> IdpUserRegisterService(UserRegistrationModel registerModel);

        /// <summary>
        /// Ckeck if admin token in valid.
        /// <summary>
        public Task<string> CheckAdminTokenIdpServiceProvider();

        /// <summary>
        /// Makes user profile request by email parameter.
        /// <summary>
        public Task<IdentityProviderUserProfileResponse> FetchUserDataByEmail(string email);
    }
}