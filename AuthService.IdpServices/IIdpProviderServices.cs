using AuthService.IdpServices.Contracts;
using AuthService.IdpServices.Contracts.Models;

namespace AuthService.IdpServices
{
    public interface IIdpProviderServices
    {   
        /// <summary>
        /// Sends login request to IDP
        /// <summary>
        public Task<IdentityProviderCridentialsResponse> UserLoginIdpProviderService(UserLoginModel loginRequest);

        /// <summary>
        /// Sends refresh token request
        /// <summary>
        public Task<IdentityProviderCridentialsResponse> RequestNewAccessToken(RefreshTokenRequest refreshToken);
    }
}