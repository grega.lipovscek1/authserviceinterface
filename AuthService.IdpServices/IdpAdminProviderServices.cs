using System.Net;
using System.Net.Http.Json;
using System.Text;
using AuthService.IdpServices.Contracts;
using AuthService.IdpServices.Contracts.Models;
using AuthService.IdpServices.IdpServices.Base;
using Common.Utility.CommonSettings;
using Common.Utility.Exceptions.HttpGeneric;
using Newtonsoft.Json;

namespace AuthService.IdpServices
{   
    internal class IsPdPrClass : IClassSett 
    {
        public const string CLASSCODE = "AS-IS-IdAdPrSe";
        public static string ToCode(string functionCode)
        {
            return (string) IsPdPrClass.CLASSCODE + " [ " + functionCode + " ]";
        }
    }
    public class IdpAdminProviderServices : IdpAdminServiceBase, IIdpAdminProviderServices
    {
        /// Access Public
        public async Task<bool> IdpUserRegisterService(UserRegistrationModel registerModel)
        {   
            HttpContent payload = base.Context<ProviderRegisterRequest>(new ProviderRegisterRequest(registerModel));

            HttpResponseMessage httpRequest = await this._httpClient.PostAsync(base.AdminAuthUsersUri(), payload);
          
            if(httpRequest.IsSuccessStatusCode)
            {   
                return true; 
            }
            else throw new BadGatewayException(IsPdPrClass.ToCode("IURS"), httpRequest.StatusCode);      
        }

        public async Task<string> CheckAdminTokenIdpServiceProvider()
        {   
            IdentityProviderCridentialsResponse package = await base.FetchAdminAccessPackage();

            if(package.access_token != null)
            {
                return package.access_token;
            }
            else throw new ISException("AS-IDPS-URIS","");
        }

        public async Task<IdentityProviderUserProfileResponse> FetchUserDataByEmail(string email)
        {   
            
            await this.FetchAdminAccessToken();
            
            HttpResponseMessage httpRequest = await this._httpClient.GetAsync(base.AdminAuthUsersEmailUri(email));

            if(httpRequest.IsSuccessStatusCode)
            {   
                List<IdentityProviderUserProfileResponse> responseResult = httpRequest.Content.ReadFromJsonAsync<List<IdentityProviderUserProfileResponse>>().Result;

                if(responseResult.Count == 0) return null;
                                                     
                return responseResult[0];
            }
            else throw new BadGatewayException("this is bad code", httpRequest.StatusCode); 
        }
        public async Task<bool> SendUserVerifyEmailRequest(Guid userId)
        {   
            HttpContent verifyContent = new StringContent(JsonConvert.SerializeObject(new { }), Encoding.UTF8, "application/json");
            
            HttpResponseMessage httpRequest = await this._httpClient.PutAsync(base.AdminAuthUsersVerifyEmailUri(userId), base.NoHttpContext());
            
            if(httpRequest.IsSuccessStatusCode)
            {
                return true;
            }
            else throw new BadGatewayException(base.AdminAuthUsersVerifyEmailUri(userId).ToString(), httpRequest.StatusCode);
        }

        public async Task<bool> UpdateUserProfile(Guid userId, ProviderChangeUserProfileRequest request)
        {   
            HttpContent providerRequest = base.Context<ProviderChangeUserProfileRequest>(request);

            await this.FetchAdminAccessToken();

            HttpResponseMessage service = await _httpClient.PutAsync(base.UserChangeProfileDataUri(userId), providerRequest);

            if (service.StatusCode == HttpStatusCode.NoContent)
            {
                return true;
            }
            else throw new BadGatewayException(IsPdPrClass.ToCode("UUP"), service.StatusCode);

        }
    /// <class>
    }
}