namespace AuthService.IdpServices.Contracts
{
    public record RefreshTokenRequest
    {
        public string RefreshToken { get; set; }

        public RefreshTokenRequest(string refreshToken)
        {
            this.RefreshToken = refreshToken;
        }
    }
}
