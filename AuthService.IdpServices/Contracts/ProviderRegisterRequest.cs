
using AuthService.IdpServices.Contracts.Models;

namespace AuthService.IdpServices.Contracts;

public class IndentityUserCredentials
{
    public string type { get; set; } = "password";
    public string value { get; set; }
    public bool temporary { get; set; } = false;
    public IndentityUserCredentials(string passwordValue)
    {
        value = passwordValue;
    }

}

public class ProviderRegisterRequest
{
    public bool enabled { get; set; } = true;
    public string username { get; set; }
    public string email { get; set; }
    public string? firstName { get; set; }
    public string? lastName { get; set; }
    public List<string> groups { get; set; }
    public List<IndentityUserCredentials> credentials { get; set; }

    public ProviderRegisterRequest(UserRegistrationModel request)
    {
        username = request.Email;
        email = request.Email;
        firstName = request.FirstName;
        lastName = request.LastName;

        credentials = new List<IndentityUserCredentials>()
        {
            new IndentityUserCredentials(request.Password)
        };
    }
}
