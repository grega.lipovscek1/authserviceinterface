namespace AuthService.IdpServices.Contracts
{
    public record WellKnownModel 
    {
        public string Issuer { get; set; }

        public string Authorization_endpoint { get; set; }

        public string Token_endpoint { get; set; }

        public string Introspection_endpoint { get; set; }

        public string Userinfo_endpoint { get; set; }

        public string End_session_endpoint { get; set; }

        public string Jwks_uri { get; set; }

        public string Check_session_iframe { get; set; }


    }
}