namespace AuthService.IdpServices.Contracts
{
    public record ProviderChangeUserProfileRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}


