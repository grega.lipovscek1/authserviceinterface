using AuthService.IdpServices.Contracts.Models;

namespace AuthService.IdpServices.Contracts
{
    public record UserRegistrationRequest
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public UserRegistrationRequest(UserRegistrationModel model)
        {
            this.UserName = model.UserName;

            this.Email = model.Email;

            this.Password = model.Password;
        }
    }
}