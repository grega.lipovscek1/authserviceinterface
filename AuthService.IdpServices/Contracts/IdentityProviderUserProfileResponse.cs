namespace AuthService.IdpServices.Contracts;
public class UserProfileAccess
{
    public bool ManageGroupMembership { get; set; }
    public bool View { get; set; }
    public bool MapRoles { get; set; }
    public bool Impersonate { get; set; }
    public bool Manage { get; set; }
}

public class IdentityProviderUserProfileResponse
{
    public Guid Id { get; set; }
    public long CreatedTimestamp { get; set; }
    public string Username { get; set; }
    public bool Enabled { get; set; }
    public bool Totp { get; set; }
    public bool EmailVerified { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public List<string> DisableableCredentialTypes { get; set; }
    public List<string> RequiredActions { get; set; }
    public long NotBefore { get; set; }
    public UserProfileAccess Access { get; set; }
}
