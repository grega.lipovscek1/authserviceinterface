using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using AuthService.IdpServices.Contracts;
using Common.Utility.Exceptions.HttpGeneric;
using Common.Utility.Exceptions.SystemGeneric;

namespace AuthService.IdpServices.IdpServices.Base
{
    public abstract class IdpAdminServiceBase : IdpServiceBase
    {   
        
        /// Access protected
        protected Uri AdminAuthUsersEmailUri(string email)
        {   
            this.ConfigureAdminAuthEndpoint();
            
            return new Uri(this._isshuer + "/users?email=" + email);
        }
        protected Uri AdminAuthUsersVerifyEmailUri(Guid userId)
        {   
            this.ConfigureAdminAuthEndpoint();
            
            return new Uri(this._isshuer + "/users/" + userId + "/send-verify-email");
        }
        
        protected Uri UserChangeProfileDataUri(Guid userId)
        {   
            this.ConfigureAdminAuthEndpoint();       
            return new Uri(this._isshuer + "/users/" + userId);
        }

        /// <summary>
        /// Makes admin access token request and add it to headers of next request.
        /// <summary>
        protected async Task<bool> FetchAdminAccessToken()
        {    
            IdentityProviderCridentialsResponse adminTokenResponse = await this.MakeAdminTokenRequest();

            if(!string.IsNullOrEmpty(adminTokenResponse.access_token))
            {
                this._httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", adminTokenResponse.access_token);

                return true;
            }
            else throw new InternalServerException("No access token content!"); 
        }

        protected async Task<IdentityProviderCridentialsResponse> FetchAdminAccessPackage()
        {   
            return await this.MakeAdminTokenRequest();
        }

        /// Access private
        private string _grant_type { get; set; } = "client_credentials";

        /// <summary>
        /// Form url encoded content. Fetch admin token payload.
        /// <summary>
        private FormUrlEncodedContent FormRegistrationEncodedUrl()
        {
            var formUrl = new[]
            {
                new KeyValuePair<string, string>("client_id", base._settings.ClientAdmin),
                new KeyValuePair<string, string>("client_secret", base._settings.ClientAdminSecret),
                new KeyValuePair<string, string>("grant_type", _grant_type)
            };
            return new FormUrlEncodedContent(formUrl);
        }

        /// Access public
        /// <summary>
        /// Fetch admin token. 
        /// <summary>        
        private async Task<IdentityProviderCridentialsResponse> MakeAdminTokenRequest()
        {   
            HttpResponseMessage httpRequest = await this._httpClient.PostAsync(await base.TokenEnpoint(), this.FormRegistrationEncodedUrl());


            if(httpRequest.IsSuccessStatusCode)
            {   
                if(httpRequest.Content != null)
                {
                    IdentityProviderCridentialsResponse responseResult = httpRequest.Content.ReadFromJsonAsync<IdentityProviderCridentialsResponse>().Result;
                    
                    return responseResult;
                }
                else throw new InternalServerException("No content");   
            }
            else throw new BadGatewayException("As-IdpS::IdpAdmS.B"+ await base.TokenEnpoint(), httpRequest.StatusCode);   
            
        }

        

        /// <class>
    }
}
