using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using AuthService.IdpServices.Contracts;
using Common.Utility.Exceptions.HttpGeneric;
using Newtonsoft.Json;

namespace AuthService.IdpServices.IdpServices.Base
{   
    /// <summary>
    /// Basic idp serivce abstract class
    /// <summary>
    public abstract class IdpServiceBase
    {   
        /// Access protected
        protected string _isshuer { get; set; }
        protected HttpClient _httpClient { get; set; }
        protected WellKnownModel _wellKnown { get; set; }
        protected string _client { get; set; }
        protected IdpSettings _settings { get; set; }

        /// Access private
        private string _uri { get; set; }
        private string _realm { get; set; }

        /// <summary>
        /// Confugure a basic identity provider settings
        /// <summary>
        public void Configure(IdpSettings settings)
        {   
            this._settings = settings;

            this._uri = settings.Uri;

            this._realm = settings.Realm;

            this._client = settings.Client_id;

            this.ConfigureAuthEndpoint();

            this._httpClient = new HttpClient();

        }

        /// <summary>
        /// Configures a basic indentity provider enpoint.
        /// etc.:  https://auth.example.com/auth/realms/EX-local
        /// <summary>
        private void ConfigureAuthEndpoint()
        {
            this._isshuer = this._uri 
                            + "/realms/"
                            + this._realm;
        }

        /// <summary>
        /// Configures admin indentity provider enpoint.
        /// etc.:  https://auth.example.com/auth/admin/realms/EX-local
        /// <summary>
        protected void ConfigureAdminAuthEndpoint()
        {
            this._isshuer = this._uri
                            + "/admin" 
                            + "/realms/"
                            + this._realm;
        }
        protected Uri AdminAuthUsersUri()
        {   
            this.ConfigureAdminAuthEndpoint();
            
            return new Uri(this._isshuer + "/users");
        }

        /// <summary>
        /// Forms token endpoint from well known source.
        /// <summary>
        protected async Task<Uri> TokenEnpoint()
        {   
            if(await this.GetWellKnowSpacificationRequest() == true)
            {
                return new Uri(_wellKnown.Token_endpoint);
            }
            else throw new InternalServerException("AS-IDPS-IS.B-TEU");
        }

        protected HttpContent Context<T>(T payload)
        {
            HttpContent contentToPost = new StringContent(JsonConvert.SerializeObject(payload), Encoding.UTF8, "application/json");
            return contentToPost;
        }
        protected HttpContent NoHttpContext()
        {
            return new StringContent(JsonConvert.SerializeObject(new {}), Encoding.UTF8, "application/json");
        }

        /// <summary>
        /// Configure a basic identity provider enpoint.
        /// etc.: https://auth.example.com/auth/realms/EX-local/.well-known/openid-configuration
        /// <summary>
        public string GetWellKnownEnpointUri()
        {     
            return this._isshuer + IIdpServiceEndpoints.WELL_KNOWN;
        }

        /// <summary>
        /// Request all data from well-known identity provider public api.
        /// <summary>
        private async Task<bool> GetWellKnowSpacificationRequest()
        {   
            HttpResponseMessage httpRequest = await this._httpClient.GetAsync(this.GetWellKnownEnpointUri());

            if(httpRequest.StatusCode == HttpStatusCode.Conflict) throw new BadGatewayException("AS-IDPS-.BASE",HttpStatusCode.Conflict);

            if(httpRequest.IsSuccessStatusCode)
            {
                WellKnownModel responseResult = httpRequest.Content.ReadFromJsonAsync<WellKnownModel>().Result;

                if(httpRequest.Content != null)
                {
                    this._wellKnown =  responseResult;

                    return true;
                }
                else throw new InternalServerException("AS-IDPS-IS.B-ISB");            
            }
            else throw new BadGatewayException("AS-IDPS-IS.B-ISB", httpRequest.StatusCode);       
        }
    }
}