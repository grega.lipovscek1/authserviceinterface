namespace AuthService.IdpServices.IdpServices.Base
{
    public class IdpSettings
    {
        public string Uri { get; set; }
        public string Realm { get; set; }
        public string Client_id { get; set; }
        public string ClientAdmin { get; set; }
        public string ClientAdminSecret { get; set; }

        public IdpSettings
        (
            string uri, 
            string realm, 
            string client_id,
            string clientAdmin,
            string clientAdminSecret
        )
        {
            this.Uri = uri;

            this.Realm = realm;

            this.Client_id = client_id;

            this.ClientAdmin = clientAdmin;

            this.ClientAdminSecret = clientAdminSecret;
        }
    }
}