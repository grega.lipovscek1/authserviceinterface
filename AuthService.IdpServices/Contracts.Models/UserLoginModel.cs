namespace AuthService.IdpServices.Contracts.Models
{
    public record UserLoginModel
    {
        public string? UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public UserLoginModel(string email, string password, string? userName)
        {
            this.Email = email;

            this.Password = password;

            this.UserName = userName;
        }
        
    }

}