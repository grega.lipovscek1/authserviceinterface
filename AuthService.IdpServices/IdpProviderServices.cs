using System.Net;
using System.Net.Http.Json;
using AuthService.IdpServices.Contracts;
using AuthService.IdpServices.Contracts.Models;
using AuthService.IdpServices.IdpServices.Base;
using Common.Utility.CommonSettings;
using Common.Utility.Exceptions.HttpGeneric;

namespace AuthService.IdpServices
{   
    internal class ContRegClass : IClassSett 
    {
        public const string CLASSCODE = "AS-Idp-Log";
        public static string ToCode(string functionCode)
        {
            return (string) ContRegClass.CLASSCODE + " [ " + functionCode + " ]";
        }
    }

    public class IdpProviderServices : IdpServiceBase, IIdpProviderServices
    {
        public async Task<IdentityProviderCridentialsResponse> UserLoginIdpProviderService(UserLoginModel loginRequest)
        {   
            HttpResponseMessage providerResponse = await _httpClient.PostAsync(await base.TokenEnpoint(),
                                this.FormUserCridentialsUrl(loginRequest)
                                );

            if (providerResponse.StatusCode == HttpStatusCode.Unauthorized) throw new UnauthorizedException("Wrong client credentials!");

            if (providerResponse.StatusCode == HttpStatusCode.BadRequest) throw new BadRequestException("Account is not activated!");

            if (providerResponse.StatusCode == HttpStatusCode.Conflict) throw new ConflictException("Conflict - unhandled error!");

            if (!providerResponse.IsSuccessStatusCode) throw new BadRequestException("Something else went wrong!");

            if (providerResponse.Content != null)
            {
                IdentityProviderCridentialsResponse response = providerResponse.Content.ReadFromJsonAsync<IdentityProviderCridentialsResponse>().Result;

                return response;
            }
            else throw new InternalServerException("IdentityProviderLogin!");
            

           
        }

        protected FormUrlEncodedContent FormUserCridentialsUrl(UserLoginModel loginRequest)
        {
            var formClientCridentials = new[]
            {
                new KeyValuePair<string, string>("client_id", base._client),
                new KeyValuePair<string, string>("username", loginRequest.Email),
                new KeyValuePair<string, string>("password", loginRequest.Password),
                new KeyValuePair<string, string>("grant_type", "password")
            };
            return new FormUrlEncodedContent(formClientCridentials);
        }

        public async Task<IdentityProviderCridentialsResponse> RequestNewAccessToken(RefreshTokenRequest refreshToken)
        {
            HttpResponseMessage providerResponse = await _httpClient.PostAsync(await base.TokenEnpoint()
                                            , this.FormUserRefreshTokenEncodedUrl(refreshToken)
                                            );

            if (providerResponse.StatusCode == HttpStatusCode.BadRequest) throw new UnauthorizedException("Refresh token is invalid!");

            if (providerResponse.StatusCode == HttpStatusCode.Unauthorized) throw new BadGatewayException(ContRegClass.ToCode("E.50206"),HttpStatusCode.Unauthorized);
        
            if (providerResponse.StatusCode == HttpStatusCode.Conflict) throw new BadGatewayException(ContRegClass.ToCode("E.50206"),HttpStatusCode.Conflict);

            if (providerResponse.IsSuccessStatusCode)
            {
                if (providerResponse.Content != null)
                {
                    IdentityProviderCridentialsResponse response = providerResponse.Content.ReadFromJsonAsync<IdentityProviderCridentialsResponse>().Result;

                    return response;
                }
                else { throw new InternalServerException("RC-IDP-RT"); }
            }
            throw new BadGatewayException(ContRegClass.ToCode("E.50206"),providerResponse.StatusCode); 

            
        }

        protected FormUrlEncodedContent FormUserRefreshTokenEncodedUrl(RefreshTokenRequest request)
        {
            var formClientCridentials = new[]
           {
                new KeyValuePair<string, string>("client_id", this._client),
                new KeyValuePair<string, string>("grant_type", "refresh_token"),
                new KeyValuePair<string, string>("refresh_token", request.RefreshToken)
            };
            return new FormUrlEncodedContent(formClientCridentials);
        }

        

    /// <class>    
    }
}