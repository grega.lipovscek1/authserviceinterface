using AuthService.Core.Login.Services;
using AuthService.Core.Login.Services.Register.Services;
using AuthService.Core.User.Services;
using AuthService.Core.Utility.Services;
using AuthService.DataAccess;
using AuthService.DataAccess.Providers;
using AuthService.IdpServices;
using AuthService.WebApi.Extentions;
using Common.Utility.AccessControl;
using Common.Utility.CommonSettings;
using runchicken_be.Extensions;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddAccessControl(builder.Configuration.GetSection("AppSettings:Security"));

builder.Services.Configure<AppSettings>(builder.Configuration.GetSection("AppSettings"));



// Add database context. DataAccess solution.
builder.Services.AddDbContext<DatabaseContext>();

builder.Services.AddSingleton<ApplicationDbContext>();


// Add scoped services
builder.Services.AddSingleton<RegisterService>();
builder.Services.AddSingleton<LoginService>();
builder.Services.AddSingleton<AccountEntityProvider>();
builder.Services.AddSingleton<IdpAdminProviderServices>();
builder.Services.AddSingleton<UtilityIdpService>();
builder.Services.AddSingleton<IdpProviderServices>();
builder.Services.AddSingleton<UserServices>();













var app = builder.Build();
await AuthServiceSetup.Initialize(app);
// Using exception middleweare!
app.ConfigureCustomExceptionMiddleware();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
