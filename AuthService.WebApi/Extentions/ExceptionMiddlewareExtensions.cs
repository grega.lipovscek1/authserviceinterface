using Common.Utility.CommonResponses;
using Common.Utility.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging.Abstractions;
using System.Net;

namespace runchicken_be.Extensions;

public static class ExceptionMiddlewareExtensions
{
    //Exception middleware, that populates status code and content type of our response,
    //logs the errr and returns response as ErrorDetails object
    public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILogger logger)
    {
        app.UseExceptionHandler(appError =>
        {
            appError.Run(async context =>
            {
                context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                context.Response.ContentType = "application/json";
                var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                if (contextFeature != null)
                {
                    logger.LogError($"Something went wrong: {contextFeature.Error}");
                    // await context.Response.WriteAsync(new ErrorDetails()
                    // {
                    //     // ErrorCode = context.Response.StatusCode,
                    //     Message = "Internal Server Error."
                    // }.ToString());
                }
            });
        });
    }

    //Configuration for custom exception middleware
    public static void ConfigureCustomExceptionMiddleware(this WebApplication app)
    {

        app.UseMiddleware<ExceptionMiddleware>();
    }
}

