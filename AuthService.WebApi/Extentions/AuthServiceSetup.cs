using AuthService.DataAccess;
using Common.Utility.CommonSettings;
using Common.Utility.CommonSetup;
using Microsoft.Extensions.Options;

namespace AuthService.WebApi.Extentions;

public class AuthServiceSetup : Setup
{
    public static async Task Initialize(WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                var settings = scope.ServiceProvider.GetService<IOptions<AppSettings>>()!.Value;
                //TODO: Add validation for settings
                //Validate database conneciton and integrity
                Console.Write("Checking database configuration and running migrations...");
                var db = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
                //await db.Database.MigrateAsync();
                //await SeedTriggers(db, settings);
            }
        }
}