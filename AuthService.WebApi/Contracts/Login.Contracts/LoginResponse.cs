namespace AuthService.WebApi.Contracts.Login.Contracts;

public record LoginResponse
{
    public string access_token { set; get; }
    public int expires_in { set; get; }
    public int refresh_expires_in { set; get; }
    public string refresh_token { set; get; }
    public string token_type { set; get; }
    public string session_state { set; get; }
    public string scope { set; get; }

}
