namespace AuthService.WebApi.Contracts.Register.Contracts
{
    public record UserRegisterRequest
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        
    }
}