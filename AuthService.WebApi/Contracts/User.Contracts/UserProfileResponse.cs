using AuthService.DataAccess.Entities;

namespace AuthService.WebApi.Contracts.User.Contracts
{
    public record UserProfileResponse
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public UserProfileResponse(AccountEntity account)
        {
            this.UserId = account.UserId;

            this.UserName = account.UserName;

            this.FirstName = account.FirstName;

            this.LastName = account.LastName;
        }    
    }
}