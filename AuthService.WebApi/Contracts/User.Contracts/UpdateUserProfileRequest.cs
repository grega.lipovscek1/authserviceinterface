namespace AuthService.WebApi.Contracts.User.Contracts
{
    public record UpdateUserProfileRequest
    {   
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}