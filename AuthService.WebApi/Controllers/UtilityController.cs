using AuthService.Core.Login.Services;
using AuthService.Core.Models;
using AuthService.Core.Utility.Services;
using AuthService.WebApi.Contracts.Login.Contracts;
using Common.Utility.Exceptions.HttpGeneric;
using Microsoft.AspNetCore.Mvc;

namespace AuthService.Controllers;

[ApiController]
[Route("[controller]")]
public class UtilityController : ControllerBase
{   
    /// Access Private
    private readonly LoginService _loginService;
    private readonly UtilityIdpService _utilityIdpService;

    /// Constructor ***
    public UtilityController
    (
        LoginService loginService,
        UtilityIdpService utilityIdpService
    )
    {
        this._loginService = loginService;
        this._utilityIdpService = utilityIdpService;
    }

    /// Access public

    /// <summary>
    /// Basic utility get check!
    /// </summary>
    /// <returns>void</returns>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(void))]
    public async Task<ActionResult> HandleUtilityBaseCall()
    {
        return Ok();
    }

    /// <summary>
    /// Basic health check
    /// </summary>
    /// <returns>string</returns>
    [HttpGet]
    [Route("HealthCheck")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
    public async Task<ActionResult> HandleUtilityHealthCheck()
    {   
        return Ok("HealthCheck");
    }

    /// <summary>
    /// Sends request for admin-cli token.
    /// <summary>
    /// <response code="500">Sometnig else went wrong!</response>
    [HttpGet]
    [Route("AdminAccess")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
    public async Task<ActionResult<string>> HandleAdminAccessCheck()
    {   
        string response = await this._utilityIdpService.CheckAdminAccessTokenService();

        if(response != null)
        {
            return Ok(response);
        }
        else throw new InternalServerException("Sometnig else went wrong!");
    }

    [HttpGet]
    [Route("User/Profile/{email}")]
    public async Task<UserProfileModel> HandleUtilityPostRequest(string email)
    {   
        if(string.IsNullOrEmpty(email)) throw new InternalServerException("Sometnig else went wrong!");

        UserProfileModel response = await this._utilityIdpService.FetchUserProfileByEmail(email);

        return response;
        
    }

}