using System.Net;
using AuthService.Core.Login.Services.Register.Services;
using AuthService.IdpServices.Contracts;
using AuthService.WebApi.Contracts.Register.Contracts;
using Common.Utility.CommonSettings;
using Common.Utility.Exceptions.HttpGeneric;
using Common.Utility.Exceptions.SystemGeneric;
using Microsoft.AspNetCore.Mvc;

namespace AuthService.WebApi.Controllers;

internal class ContRegClass : IClassSett 
{
    public const string CLASSCODE = "AS-Con-Reg";
    public static string ToCode(string functionCode)
    {
        return (string) ContRegClass.CLASSCODE + " [ " + functionCode + " ]";
    }
}

[ApiController]
[Route("[controller]")]
public class RegisterController : ControllerBase
{   
    private readonly RegisterService _registerService;

    public RegisterController
    (
        RegisterService registerService
    )
    {
        this._registerService = registerService;
    }

    /// <summary>
    /// Register new user.
    /// <summary>
    /// <response code="400">Missing email</response>
    /// <response code="400">Missing password</response>
    /// <response code="409">User already exists</response>
    /// <response code="502">AS-Core-R.S-ReSe [E.50206]</response>
    /// <response code="500">AS-Con-Reg [E.50206]</response>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(IdentityProviderUserProfileResponse))]
    public async Task<ActionResult> HandleUserRegisterRequest(UserRegisterRequest? request)
    {   
        if(string.IsNullOrEmpty(request.Email)) throw new ApiException("Missing email!", HttpStatusCode.BadRequest);
        
        if(string.IsNullOrEmpty(request.Password)) throw new ApiException("Missing password!", HttpStatusCode.BadRequest);
        
        IdentityProviderUserProfileResponse userProfile = await this._registerService.UserRegisterService(request.Email, request.Password, request.UserName);

        return Ok(userProfile);      
    }

    /// <summary>
    /// Sends request for admin-cli token.
    /// <summary>
    /// <response code="405">Method not Allowed</response>
    /// <response code="500">AS-Con-Reg [E.50206]</response>
    [HttpGet]
    [Route("{email}")]
    [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(void))]
    public async Task<ActionResult> HandleResendVerifyEmailRequest(string email)
    {   
        if(string.IsNullOrEmpty(email)) throw new ApiException("Missing email!", HttpStatusCode.Conflict);

        if(await this._registerService.ResendUserVerifyEmail(email) == true)
        {
            return Accepted();
        }
        else throw new InternalServerException(ContRegClass.ToCode("E.50206"));    
    }
}