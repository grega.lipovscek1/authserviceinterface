
using AuthService.Core.User.Services;
using AuthService.DataAccess.Entities;
using AuthService.WebApi.Contracts.User.Contracts;
using Common.Utility.AccessControl;
using Common.Utility.CommonSettings;
using Common.Utility.Exceptions.HttpGeneric;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace AuthService.WebApi.Controllers;

internal class ContUserClass : IClassSett 
{
    public const string CLASSCODE = "AS-Con-Usr";
    public static string ToCode(string functionCode)
    {
        return (string) ContUserClass.CLASSCODE + " [ " + functionCode + " ]";
    }
}

[Authorize]
[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{   
    private readonly UserServices _userServices;
    public UserController(UserServices userServices)
    {
        this._userServices = userServices;
    }

    /// <summary>
    /// Returns user profile
    /// <summary>
    /// <response code="409">User with this id do not exists in local database</response>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(UserProfileResponse))]
    public async Task<ActionResult<UserProfileResponse>> HandleUserData()
    {    
        AccessUserModel User = UserControl.GetUserData(Request.Headers.Authorization);

        AccountEntity account = await this._userServices.GetUserDataById(User.UserId);

        UserProfileResponse response = new UserProfileResponse(account);

        return response;     
    }

    /// <summary>
    /// Returns user profile
    /// <summary>
    /// <response code="502">Connection to identity proveder failed</response>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(UserProfileResponse))]
    public async Task<ActionResult> HandleUpdateProfileRequest(UpdateUserProfileRequest request)
    {   
        AccessUserModel User = UserControl.GetUserData(Request.Headers.Authorization);

        if(await this._userServices.UpdateUserProfileService(User.UserId, request.FirstName, request.LastName) == true)
        {
            return Accepted();
        }
        else throw new InternalServerException(ContUserClass.ToCode("E.50206"));
    }
}