using AuthService.WebApi.Contracts.Login.Contracts;
using Microsoft.AspNetCore.Mvc;
using AuthService.Core.Login.Services;
using AuthService.IdpServices.Contracts;
using Common.Utility.Exceptions.HttpGeneric;
using Common.Utility.CommonSettings;
using Microsoft.AspNetCore.Authorization;

namespace AuthService.Controllers;

internal class ContRegClass : IClassSett 
{
    public const string CLASSCODE = "AS-Con-Log";
    public static string ToCode(string functionCode)
    {
        return (string) ContRegClass.CLASSCODE + " [ " + functionCode + " ]";
    }
}

[ApiController]
[Route("[controller]")]
public class LoginController : ControllerBase
{   
    private readonly LoginService _loginService;
    public LoginController
    (
        LoginService loginService
    )
    {
        this._loginService = loginService;
    }

    
    [HttpPost]
    public async Task<ActionResult<LoginResponse>> HandleGetUserRequest(LoginRequest request)
    {   
        if(string.IsNullOrEmpty(request.UserName)) throw new BadRequestException("Missing username");

        if(string.IsNullOrEmpty(request.Password)) throw new BadRequestException("Missing password");

        IdentityProviderCridentialsResponse response = 
                    await this._loginService.UserLoginService(
                        request.UserName,
                        request.Password
                    );

        if(response != null)
        {
            return Ok(response);
        }
        else throw new InternalServerException(ContRegClass.ToCode("E.50206"));
        
    }

    /// <summary>
    /// Takes refresh token from header (Bearer refreshToken) and 
    /// rerutes it to IDP provider
    /// <summary>
     /// <response code="400">Missing refresh token!</response>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
    public async Task<ActionResult<IdentityProviderCridentialsResponse>> HandleRefreshTokenRequest()
    {   
        if(!string.IsNullOrEmpty(Request.Headers.Authorization))
        {
            string authorisationHeader = Request.Headers.Authorization;
            string[] bearer = authorisationHeader.Split(" ");
            string refreshToken = bearer[1];
            
            IdentityProviderCridentialsResponse response = await this._loginService.RefreshTokenService(refreshToken);
            return Ok(response);
        }
        else throw new BadRequestException("Missing refresh token");
        
    }
}