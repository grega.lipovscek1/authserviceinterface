using AuthService.DataAccess.Entities;
using AuthService.DataAccess;
using Common.Utility.CommonSettings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace AuthService.DataAccess
{
    public class DatabaseContext : ApplicationDbContext
    {   
        public DatabaseContext(IOptions<AppSettings> settings)
        : base(settings)
        {
        }
        


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountEntity>()
                .Property(a => a.CreatedAt)
                .ValueGeneratedOnAdd()
                .HasDefaultValueSql("NOW()");
            base.OnModelCreating(modelBuilder);
        }

    }

    
}